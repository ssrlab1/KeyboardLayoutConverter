<?php
	class KeyboardLayoutConverter {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $text = '';
		private $correspondences = array();
		private $result = '';
		const BR = "<br>\n";
		
		function __construct() {
			$this->correspondences = array();
			$this->correspondences[] = array('en' => '`', 'be' => 'ё', 'ru' => 'ё');
			$this->correspondences[] = array('en' => '1', 'be' => '1', 'ru' => '1');
			$this->correspondences[] = array('en' => '2', 'be' => '2', 'ru' => '2');
			$this->correspondences[] = array('en' => '3', 'be' => '3', 'ru' => '3');
			$this->correspondences[] = array('en' => '4', 'be' => '4', 'ru' => '4');
			$this->correspondences[] = array('en' => '5', 'be' => '5', 'ru' => '5');
			$this->correspondences[] = array('en' => '6', 'be' => '6', 'ru' => '6');
			$this->correspondences[] = array('en' => '7', 'be' => '7', 'ru' => '7');
			$this->correspondences[] = array('en' => '8', 'be' => '8', 'ru' => '8');
			$this->correspondences[] = array('en' => '9', 'be' => '9', 'ru' => '9');
			$this->correspondences[] = array('en' => '0', 'be' => '0', 'ru' => '0');
			$this->correspondences[] = array('en' => '-', 'be' => '-', 'ru' => '-');
			$this->correspondences[] = array('en' => '=', 'be' => '=', 'ru' => '=');
			$this->correspondences[] = array('en' => 'q', 'be' => 'й', 'ru' => 'й');
			$this->correspondences[] = array('en' => 'w', 'be' => 'ц', 'ru' => 'ц');
			$this->correspondences[] = array('en' => 'e', 'be' => 'у', 'ru' => 'у');
			$this->correspondences[] = array('en' => 'r', 'be' => 'к', 'ru' => 'к');
			$this->correspondences[] = array('en' => 't', 'be' => 'е', 'ru' => 'е');
			$this->correspondences[] = array('en' => 'y', 'be' => 'н', 'ru' => 'н');
			$this->correspondences[] = array('en' => 'u', 'be' => 'г', 'ru' => 'г');
			$this->correspondences[] = array('en' => 'i', 'be' => 'ш', 'ru' => 'ш');
			$this->correspondences[] = array('en' => 'o', 'be' => 'ў', 'ru' => 'щ');
			$this->correspondences[] = array('en' => 'p', 'be' => 'з', 'ru' => 'з');
			$this->correspondences[] = array('en' => '[', 'be' => 'х', 'ru' => 'х');
			$this->correspondences[] = array('en' => ']', 'be' => "'", 'ru' => 'ъ');
			$this->correspondences[] = array('en' => 'a', 'be' => 'ф', 'ru' => 'ф');
			$this->correspondences[] = array('en' => 's', 'be' => 'ы', 'ru' => 'ы');
			$this->correspondences[] = array('en' => 'd', 'be' => 'в', 'ru' => 'в');
			$this->correspondences[] = array('en' => 'f', 'be' => 'а', 'ru' => 'а');
			$this->correspondences[] = array('en' => 'g', 'be' => 'п', 'ru' => 'п');
			$this->correspondences[] = array('en' => 'h', 'be' => 'р', 'ru' => 'р');
			$this->correspondences[] = array('en' => 'j', 'be' => 'о', 'ru' => 'о');
			$this->correspondences[] = array('en' => 'k', 'be' => 'л', 'ru' => 'л');
			$this->correspondences[] = array('en' => 'l', 'be' => 'д', 'ru' => 'д');
			$this->correspondences[] = array('en' => ';', 'be' => 'ж', 'ru' => 'ж');
			$this->correspondences[] = array('en' => "'", 'be' => 'э', 'ru' => 'э');
			$this->correspondences[] = array('en' => '\'', 'be' => '\'', 'ru' => '\'');
			$this->correspondences[] = array('en' => 'z', 'be' => 'я', 'ru' => 'я');
			$this->correspondences[] = array('en' => 'x', 'be' => 'ч', 'ru' => 'ч');
			$this->correspondences[] = array('en' => 'c', 'be' => 'с', 'ru' => 'с');
			$this->correspondences[] = array('en' => 'v', 'be' => 'м', 'ru' => 'м');
			$this->correspondences[] = array('en' => 'b', 'be' => 'і', 'ru' => 'и');
			$this->correspondences[] = array('en' => 'n', 'be' => 'т', 'ru' => 'т');
			$this->correspondences[] = array('en' => 'm', 'be' => 'ь', 'ru' => 'ь');
			$this->correspondences[] = array('en' => ',', 'be' => 'б', 'ru' => 'б');
			$this->correspondences[] = array('en' => '.', 'be' => 'ю', 'ru' => 'ю');
			$this->correspondences[] = array('en' => '/', 'be' => '.', 'ru' => '.');
			$this->correspondences[] = array('en' => '~', 'be' => 'Ё', 'ru' => 'Ё');
			$this->correspondences[] = array('en' => 'Q', 'be' => 'Й', 'ru' => 'Й');
			$this->correspondences[] = array('en' => 'W', 'be' => 'Ц', 'ru' => 'Ц');
			$this->correspondences[] = array('en' => 'E', 'be' => 'У', 'ru' => 'У');
			$this->correspondences[] = array('en' => 'R', 'be' => 'К', 'ru' => 'К');
			$this->correspondences[] = array('en' => 'T', 'be' => 'Е', 'ru' => 'Е');
			$this->correspondences[] = array('en' => 'Y', 'be' => 'Н', 'ru' => 'Н');
			$this->correspondences[] = array('en' => 'U', 'be' => 'Г', 'ru' => 'Г');
			$this->correspondences[] = array('en' => 'I', 'be' => 'Ш', 'ru' => 'Ш');
			$this->correspondences[] = array('en' => 'O', 'be' => 'Ў', 'ru' => 'Щ');
			$this->correspondences[] = array('en' => 'P', 'be' => 'З', 'ru' => 'З');
			$this->correspondences[] = array('en' => '{', 'be' => 'Х', 'ru' => 'Х');
			$this->correspondences[] = array('en' => '}', 'be' => "'", 'ru' => 'Ъ');
			$this->correspondences[] = array('en' => 'A', 'be' => 'Ф', 'ru' => 'Ф');
			$this->correspondences[] = array('en' => 'S', 'be' => 'Ы', 'ru' => 'Ы');
			$this->correspondences[] = array('en' => 'D', 'be' => 'В', 'ru' => 'В');
			$this->correspondences[] = array('en' => 'F', 'be' => 'А', 'ru' => 'А');
			$this->correspondences[] = array('en' => 'G', 'be' => 'П', 'ru' => 'П');
			$this->correspondences[] = array('en' => 'H', 'be' => 'Р', 'ru' => 'Р');
			$this->correspondences[] = array('en' => 'J', 'be' => 'О', 'ru' => 'О');
			$this->correspondences[] = array('en' => 'K', 'be' => 'Л', 'ru' => 'Л');
			$this->correspondences[] = array('en' => 'L', 'be' => 'Д', 'ru' => 'Д');
			$this->correspondences[] = array('en' => ':', 'be' => 'Ж', 'ru' => 'Ж');
			$this->correspondences[] = array('en' => '"', 'be' => 'Э', 'ru' => 'Э');
			$this->correspondences[] = array('en' => '|', 'be' => '/', 'ru' => '/');
			$this->correspondences[] = array('en' => 'Z', 'be' => 'Я', 'ru' => 'Я');
			$this->correspondences[] = array('en' => 'X', 'be' => 'Ч', 'ru' => 'Ч');
			$this->correspondences[] = array('en' => 'C', 'be' => 'С', 'ru' => 'С');
			$this->correspondences[] = array('en' => 'V', 'be' => 'М', 'ru' => 'М');
			$this->correspondences[] = array('en' => 'B', 'be' => 'І', 'ru' => 'И');
			$this->correspondences[] = array('en' => 'N', 'be' => 'Т', 'ru' => 'Т');
			$this->correspondences[] = array('en' => 'M', 'be' => 'Ь', 'ru' => 'Ь');
			$this->correspondences[] = array('en' => '<', 'be' => 'Б', 'ru' => 'Б');
			$this->correspondences[] = array('en' => '>', 'be' => 'Ю', 'ru' => 'Ю');
			$this->correspondences[] = array('en' => '?', 'be' => ',', 'ru' => ',');
			$this->correspondences[] = array('en' => '!', 'be' => '!', 'ru' => '!');
			$this->correspondences[] = array('en' => '@', 'be' => '"', 'ru' => '"');
			$this->correspondences[] = array('en' => '#', 'be' => '№', 'ru' => '№');
			$this->correspondences[] = array('en' => '$', 'be' => ';', 'ru' => ';');
			$this->correspondences[] = array('en' => '%', 'be' => '%', 'ru' => '%');
			$this->correspondences[] = array('en' => '^', 'be' => ':', 'ru' => ':');
			$this->correspondences[] = array('en' => '&', 'be' => '?', 'ru' => '?');
			$this->correspondences[] = array('en' => '*', 'be' => '*', 'ru' => '*');
			$this->correspondences[] = array('en' => '(', 'be' => '(', 'ru' => '(');
			$this->correspondences[] = array('en' => ')', 'be' => ')', 'ru' => ')');
			$this->correspondences[] = array('en' => '_', 'be' => '_', 'ru' => '_');
			$this->correspondences[] = array('en' => '+', 'be' => '+', 'ru' => '+');
		}
		
		public function resetValues() {
			$this->text = '';
			$this->result = '';
		}
		
		public function setText($text) {
			$this->text = $text;
		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Keyboard Layout Converter';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/KeyboardLayoutConverter/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		public function run($inputlang, $outputlang) {
			$this->result = '';
			$chars = preg_split('//u', $this->text, -1, PREG_SPLIT_NO_EMPTY);
			foreach($chars as $char) {
				$newChar = '';
				foreach($this->correspondences as $k => $v) {
					if($v[$inputlang] == $char) {
						$newChar = $v[$outputlang];
						break;
					}
				}
				if($newChar) {
					$this->result .= $newChar;
				}
				else {
					$this->result .= $char;
				}
			}
		}
		
		public function saveCacheFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$dateCode = date('Y-m-d_H-i-s', time());
			$randCode = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			$serviceName = 'KeyboardLayoutConverter';
			$sendersName = 'Keyboard Layout Converter';
			$recipient = 'corpus.by@gmail.com';
			$subject = "$sendersName from IP $ip";
			$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
			$mailBody .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$textLength = mb_strlen($this->text);
			$pages = round($textLength/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->text);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=in&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Тэкст ($textLength сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300) {
					$mailBody .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
				else {
					$mailBody .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
			}
			$mailBody .= self::BR;
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_out.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", "", $this->result);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=out&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Вынік пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$mailBody .= '<blockquote><i>' . str_replace("\n", self::BR, trim($matches[0])) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
			mail($recipient, $subject, $mailBody, $header);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mailBody, $header)));
			fclose($newFile);
		}
		
		public function getResult() {
			return $this->result;
		}
	}
?>