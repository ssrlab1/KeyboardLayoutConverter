<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$text = isset($_POST['text']) ? $_POST['text'] : 'мама';
	$inputselect = isset($_POST['inp']) ? $_POST['inp'] : 'en'; 
	$outputselect = isset($_POST['outp']) ? $_POST['outp'] : 'be'; 
	
	include_once 'KeyboardLayoutConverter.php';
	KeyboardLayoutConverter::loadLocalization($localization);
	
	$msg = '';
	if(!empty($text)) {
		$KeyboardLayoutConverter = new KeyboardLayoutConverter();
		$KeyboardLayoutConverter->setText($text);
		$KeyboardLayoutConverter->run($inputselect, $outputselect);
		$KeyboardLayoutConverter->saveCacheFiles();
		
		$result['text'] = $text;
		$result['result'] = $KeyboardLayoutConverter->getResult();
		$msg = json_encode($result);
	}
	echo $msg;
?>